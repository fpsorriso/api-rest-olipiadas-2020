package br.com.cit.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.cit.exception.MessageError;
import br.com.cit.model.Pais;
import br.com.cit.service.PaisService;

/**
 * Classe responsável por fornecedor os metodos de crud e consulta do pais.
 */
@Controller
@RequestMapping("pais")
public class PaisController {
    
    @Autowired
    private PaisService paisService;

    @PutMapping("salva")
    public ResponseEntity<?> salva(@RequestBody Pais pais) {
    	try {
    		return new ResponseEntity<Pais>(paisService.salva(pais), HttpStatus.OK);
    		
    	} catch(Exception e) {
    		return new ResponseEntity<MessageError>(new MessageError(e), HttpStatus.BAD_REQUEST);
    	}
    }
    
    @DeleteMapping("remove")
    public ResponseEntity<?> remove(@RequestParam(value = "id") int id) {
    	try {
    		paisService.remove(id);
    		return new ResponseEntity<Void>(HttpStatus.OK);
    		
    	} catch(Exception e) {
    		return new ResponseEntity<MessageError>(new MessageError(e), HttpStatus.BAD_REQUEST);
    	}
    }
    
    @RequestMapping("consulta/{id}")
    public ResponseEntity<?> consulta(@PathVariable("id") int id) {
    	try {
    		if (id != 0) {
    			return new ResponseEntity<Pais>(paisService.consulta(id), HttpStatus.OK); 
    		} else {
    			return new ResponseEntity<Collection<Pais>>(paisService.consulta(), HttpStatus.OK);
    		}
    		
    	} catch (Exception e) {
    		return new ResponseEntity<MessageError>(new MessageError(e), HttpStatus.BAD_REQUEST);
    	}
    }
}
