package br.com.cit.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.cit.exception.MessageError;
import br.com.cit.model.Modalidade;
import br.com.cit.service.ModalidadeService;

/**
 * Reponsável por fornecedor os metodos para o cadastro e consulta das modalidades.
 */
@Controller
@RequestMapping("modalidade")
public class ModalidadeController {
    
    @Autowired
    private ModalidadeService modalidadeService;
    
    @PutMapping("salva")
    public ResponseEntity<?> salva(@RequestBody Modalidade modalidade) {
    	try {
    		return new ResponseEntity<Modalidade>(modalidadeService.salva(modalidade), HttpStatus.OK);
    		
    	} catch (Exception e) {
    		return new ResponseEntity<MessageError>(new MessageError(e), HttpStatus.BAD_REQUEST);
    	}
    }
    
    @DeleteMapping("remove")
    public ResponseEntity<?> remove(@RequestParam(value="id") int id) {
        try {
        	modalidadeService.remove(id);
        	return new ResponseEntity<Void>(HttpStatus.OK);
        	
        } catch (Exception e) {
        	return new ResponseEntity<MessageError>(new MessageError(e), HttpStatus.BAD_REQUEST);
        }
    }
    
    @RequestMapping("consulta/{id}")
    public ResponseEntity<?> consulta(@PathVariable("id") int id) {
    	try {
    		if (id != 0) {
    			return new ResponseEntity<Modalidade>(modalidadeService.consulta(id), HttpStatus.OK);
    		} else {
    			return new ResponseEntity<Collection<Modalidade>>(modalidadeService.consulta(), HttpStatus.OK);
    		}
    		
    	} catch (Exception e) {
    		return new ResponseEntity<MessageError>(new MessageError(e), HttpStatus.BAD_REQUEST);
    	}
    }
}
