package br.com.cit.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.cit.exception.MessageError;
import br.com.cit.model.Local;
import br.com.cit.service.LocalService;

/**
 * Classe responsável por fornecedor os metodos de crud e consulta do local da prova.
 */
@Controller
@RequestMapping("local")
public class LocalController {
    @Autowired
    private LocalService localService;

    @PutMapping("salva")
    public void salva(@RequestBody Local local) {
        localService.salva(local);
    }

    @DeleteMapping("remove")
    public ResponseEntity<?> remove(@RequestParam("id") int id) {        
        try {
            localService.remove(id);
            return new ResponseEntity<Void>(HttpStatus.OK);
            
        } catch(Exception e) {           
            return new ResponseEntity<MessageError>(new MessageError(e), HttpStatus.BAD_REQUEST);
        }
    }
    
    @RequestMapping("consulta")
    public ResponseEntity<?> consulta(@RequestParam(value = "id", defaultValue = "0") int id) {
        try {
        	if (id != 0) {
        		return new ResponseEntity<Local>(localService.consulta(id), HttpStatus.OK);
        	} else {
        		return new ResponseEntity<Collection<Local>>(localService.consulta(), HttpStatus.OK);
        	}
        	
        } catch (Exception e) {
        	return new ResponseEntity<MessageError>(new MessageError(e), HttpStatus.BAD_REQUEST);
        }
    }
}
