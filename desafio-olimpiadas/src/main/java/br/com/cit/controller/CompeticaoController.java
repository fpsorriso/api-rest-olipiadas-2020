package br.com.cit.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.cit.exception.MessageError;
import br.com.cit.model.Competicao;
import br.com.cit.model.Modalidade;
import br.com.cit.service.CompeticaoService;

/**
 * Reponsável por fornecedor os metodos para o cadastro e consulta das
 * competições.
 */
@Controller
@RequestMapping("competicao")
public class CompeticaoController {

	@Autowired
	private CompeticaoService competicaoService;

	@PutMapping("salva")
	public ResponseEntity<?> salva(@RequestBody(required = true) Competicao competicao) {
		try {
			competicaoService.salva(competicao);
			return new ResponseEntity<Competicao>(competicao, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(new MessageError(e),HttpStatus.BAD_REQUEST);
		}
	}

	@DeleteMapping("remove")
	public ResponseEntity<?> remove(@RequestParam(value = "id", required = true) int id) {
		try {
			competicaoService.remove(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
			
		} catch (Exception e) {
			return new ResponseEntity<MessageError>(new MessageError(e), HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping("consulta/{modalidade}")
	public ResponseEntity<?> consulta(@PathVariable(value = "modalidade", required = false) Modalidade modalidade) {
		try {
			return new ResponseEntity<Collection<Competicao>>(competicaoService.consulta(modalidade), HttpStatus.OK);
			
		} catch (Exception e) {
			return new ResponseEntity<MessageError>(new MessageError(e), HttpStatus.BAD_REQUEST);
		}
	}
}
