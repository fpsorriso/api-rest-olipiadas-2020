package br.com.cit.exception;

import java.io.Serializable;

public class MessageError implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String errorClass;
	private String message;
	
	/**
	 * Cria uma nova instancia de {@link MessageError}
	 * 
	 * @param errorClass
	 * @param message
	 */
	public MessageError(String errorClass, String message) {
		this.errorClass = errorClass;
		this.message = message;
	}
	
	/**
	 * Cria uma nova instancia de {@link MessageError}
	 * 
	 * @param e
	 */
	public MessageError(Exception e) {
		this(e!= null ? e.getClass().toString():"", e != null ? e.getMessage() : "");
	}
	/**
	 * @return the errorClass
	 */
	public String getErrorClass() {
		return errorClass;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	
	
}
