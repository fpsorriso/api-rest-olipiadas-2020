package br.com.cit.service;

import java.time.Duration;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cit.exception.UserException;
import br.com.cit.model.Competicao;
import br.com.cit.model.Etapa;
import br.com.cit.model.Modalidade;
import br.com.cit.repository.CompeticaoRespository;

/**
 * Classe responsável por forncedor os métodos de manipulação da competição.
 */
@Service
public class CompeticaoService {

	@Autowired
	private CompeticaoRespository repository;

	public Competicao salva(Competicao entity) {
		validaCadastro(entity);

		if (entity != null) {
			if (entity.getId() != 0 || consulta(entity.getId()) != null) {
				return repository.altera(entity);
			} else {
				return repository.insere(entity);
			}
		}

		return null;
	}

	/**
	 * Removo os dados competição.
	 * 
	 * @param pk
	 */
	public void remove(Integer pk) {
		if (pk == null || pk.equals(0)) {
			throw new UserException("Para remover, o código da competição não pode ser zero");
		}

		if (repository.consulta(pk) == null) {
			throw new UserException("Não foi encontrada a competição com o código " + pk);
		}

		repository.remove(pk);
	}

	/**
	 * Busca as competições existentes ordendas por data.
	 * 
	 * @param modalidade
	 *            <opcional>
	 * @return as competições existentes ordendas por data.
	 */
	public Collection<Competicao> consulta(Modalidade modalidade) {
		return repository.consulta(modalidade);
	}

	/**
	 * Busca a {@link Modalidade} pode ID.
	 * 
	 * @param pk
	 * @return a {@link Modalidade}
	 */
	public Competicao consulta(Integer pk) {
		return repository.consulta(pk);
	}

	private void validaCadastro(Competicao competicao) {
		if (competicao != null) {
			if (competicao.getModalidade() == null) {
				throw new UserException("Modalidade não definida para a competição");
			}

			if (competicao.getDataInicio() == null) {
				throw new UserException("Data e hora inicial da competição não informados");
			}

			if (competicao.getDataFim() == null) {
				throw new UserException("Data e hora final da competição não informados");
			}

			if (competicao.getEquipe1() == null) {
				throw new UserException("Equipe 1 não informada para a competição.");
			}

			if (competicao.getEquipe2() == null) {
				throw new UserException("Equipe 2 não informada para a competição.");
			}

			if (competicao.getEtapa() == null) {
				throw new UserException("Etapa não informada para a competição.");
			}

			if (competicao.getLocal() == null) {
				throw new UserException("Local não informado para a competição.");
			}

			if (!Etapa.SEMI_FINAL.equals(competicao.getEtapa()) && !Etapa.FINAL.equals(competicao.getEtapa())
					&& competicao.getEquipe1().equals(competicao.getEquipe2())) {
				throw new UserException(
						"A Equipe 1 e Equepe 2 não pode ser a mesma na etapa " + competicao.getEtapa().toString());
			}

			if (Duration.between(competicao.getDataInicio(), competicao.getDataFim()).getSeconds() < TimeUnit.MINUTES
					.toSeconds(30)) {
				throw new UserException("A partida não pode ter menos de 30 minutos de duração.");
			}

			if (repository.verificaCompeticaoDuplicata(competicao.getId(), competicao.getModalidade(),
					competicao.getLocal(), competicao.getDataInicio(), competicao.getDataFim())) {
				throw new UserException(String.format(
						"Já exite uma competição para a modalidade de %s no local %s no periodo de %s à %s",
						competicao.getModalidade().toString(), competicao.getLocal().toString(),
						competicao.getDataInicio().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
						competicao.getDataFim().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)));
			}
		}
	}
}
