package br.com.cit.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cit.exception.UserException;
import br.com.cit.model.Local;
import br.com.cit.repository.LocalRepository;

/**
 * Classe responsável por forncedor os métodos de manipulação do local.
 */
@Service
public class LocalService {
	
	@Autowired
	private LocalRepository repository;
	
	/**
	 * Salva os dados do {@link Local}
	 * 
	 * @param entity
	 * @return o local.
	 */
    public Local salva(Local entity) {
    	if (entity != null) {
			if (entity.getId() != 0 || consulta(entity.getId()) != null) {
				return repository.altera(entity);
			} else {
				return repository.insere(entity);
			}
		}
    	
        return null;
    }

    /**
     * Remove um local pelo ID.
     * 
     * @param pk
     */
    public void remove(Integer pk) {
    	if (pk == null || pk.equals(0)) {
			throw new UserException("Para remover, o código do local não pode ser zero");
		}

		if (repository.consulta(pk) == null) {
			throw new UserException("Não foi encontrado o local com o código " + pk);
		}

		repository.remove(pk);
        
    }

    /**
     * Consulta o local pelo ID.
     * 
     * @param pk
     * @return o local.
     */
    public Local consulta(Integer pk) {
        return repository.consulta(pk);        
    }

	/**
	 * Busca todos os locais cadastrados.
	 * 
	 * @return
	 * @see br.com.cit.repository.LocalRepository#consulta()
	 */
	public Collection<Local> consulta() {
		return repository.consulta();
	}
    
    
}
