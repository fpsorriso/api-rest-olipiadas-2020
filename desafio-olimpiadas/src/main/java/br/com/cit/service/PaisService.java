package br.com.cit.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cit.exception.UserException;
import br.com.cit.model.Pais;
import br.com.cit.repository.PaisRepository;

/**
 * Classe responsável pelo CRUD e pesquisa de pais.
 */
@Service
public class PaisService {	
    
	@Autowired
	private PaisRepository repository;
	
	/**
	 * Salva os dados do {@link Pais}
	 * 
	 * @param entity
	 * @return o {@link Pais}
	 */
    public Pais salva(Pais entity) {
    	if (entity != null) {
			if (entity.getId() != 0 || consulta(entity.getId()) != null) {
				return repository.altera(entity);
			} else {
				return repository.insere(entity);
			}
		}
    	
        return null;
    }

    /**
     * Remove o {@link Pais} pelo ID.
     * 
     * @param pk
     */
    public void remove(Integer pk) {
    	if (pk == null || pk.equals(0)) {
			throw new UserException("Para remover, o código do pais não pode ser zero");
		}

		if (repository.consulta(pk) == null) {
			throw new UserException("Não foi encontrado o pais com o código " + pk);
		}

		repository.remove(pk);
    }

    /**
     * Busca o {@link Pais} pelo ID.
     * 
     * @param pk
     * @return o {@link Pais}
     */
    public Pais consulta(Integer pk) {
        return repository.consulta(pk);
    }

	/**
	 * Busca todos os {@link Pais} cadastrados.
	 * 
	 * @return todos os {@link Pais} cadastrados.
	 * @see br.com.cit.repository.PaisRepository#consulta()
	 */
	public Collection<Pais> consulta() {
		return repository.consulta();
	}
    
    
    
}
