package br.com.cit.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cit.exception.UserException;
import br.com.cit.model.Modalidade;
import br.com.cit.repository.ModalidadeRepository;

/**
 * Classe responsável por fornecedor todos os metodos necessários para CRUD e consultas de modalidade.
 */
@Service
public class ModalidadeService {
	
	@Autowired
	private ModalidadeRepository repository;
	
	/**
	 * Salva os dados da {@link Modalidade}.
	 * 
	 * @param entity
	 * @return a {@link Modalidade}
	 */
    public Modalidade salva(Modalidade entity) {
    	if (entity != null) {
			if (entity.getId() != 0 || consulta(entity.getId()) != null) {
				return repository.altera(entity);
			} else {
				return repository.insere(entity);
			}
		}
    	
        return null;
    }

    /**
     * Remove a {@link Modalidade} pelo ID
     * 
     * @param pk
     */
    public void remove(Integer pk) {
    	if (pk == null || pk.equals(0)) {
			throw new UserException("Para remover, o código da modalidade não pode ser zero");
		}

		if (repository.consulta(pk) == null) {
			throw new UserException("Não foi encontrada a modalidade com o código " + pk);
		}

		repository.remove(pk);
    }

    /**
     * Busca a {@link Modalidade} pelo ID.
     * 
     * @param pk
     * @return a {@link Modalidade}
     */
    public Modalidade consulta(Integer pk) {
        return repository.consulta(pk);
    }


	/**
	 * Busca todas as {@link Modalidade}.
	 * 
	 * @return todas as {@link Modalidade}.
	 * @see br.com.cit.repository.ModalidadeRepository#consulta()
	 */
	public Collection<Modalidade> consulta() {
		return repository.consulta();
	}
}
