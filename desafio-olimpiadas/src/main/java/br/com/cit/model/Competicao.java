package br.com.cit.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Representa a competição
 */
@Entity
@Table(name="competicao")
public class Competicao implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int               id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="id_modalidade", nullable = false)
    private Modalidade        modalidade;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="id_local", nullable = false)
    
    private Local             local;

    private Integer           etapa;
    
    private LocalDateTime     dataInicio;
    
    private LocalDateTime     dataFim;
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="id_equipe1", nullable = false)
    private Pais              equipe1;
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="id_equipe2", nullable = false)
    private Pais              equipe2;

    /**
     * Cria uma nova instancia de Competicao.
     * 
     * @param id
     * @param modalidade
     * @param local
     * @param etapa
     * @param dataInicio
     * @param dataFim
     * @param equipe1
     * @param equipe2
     */
    public Competicao(int id, Modalidade modalidade, Local local, Etapa etapa, LocalDateTime dataInicio,
            LocalDateTime dataFim, Pais equipe1, Pais equipe2) {
        this.id = id;
        this.modalidade = modalidade;
        this.local = local;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.equipe1 = equipe1;
        this.equipe2 = equipe2;
        this.setEtapa(etapa);
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the modalidade
     */
    public Modalidade getModalidade() {
        return modalidade;
    }

    /**
     * @param modalidade
     *            the modalidade to set
     */
    public void setModalidade(Modalidade modalidade) {
        this.modalidade = modalidade;
    }

    /**
     * @return the local
     */
    public Local getLocal() {
        return local;
    }

    /**
     * @param local
     *            the local to set
     */
    public void setLocal(Local local) {
        this.local = local;
    }

    /**
     * @return the etapa
     */
    public Etapa getEtapa() {
        return Etapa.valueOf(this.etapa);
    }

    /**
     * @param etapa the etapa to set
     */
    public void setEtapa(Etapa etapa) {
        if (etapa != null) {
            this.etapa = etapa.getId();    
        } else {
            this.etapa = null;
        }
    }

    /**
     * @return the dataInicio
     */
    public LocalDateTime getDataInicio() {
        return dataInicio;
    }

    /**
     * @param dataInicio
     *            the dataInicio to set
     */
    public void setDataInicio(LocalDateTime dataInicio) {
        this.dataInicio = dataInicio;
    }

    /**
     * @return the dataFim
     */
    public LocalDateTime getDataFim() {
        return dataFim;
    }

    /**
     * @param dataFim
     *            the dataFim to set
     */
    public void setDataFim(LocalDateTime dataFim) {
        this.dataFim = dataFim;
    }

    /**
     * @return the equipe1
     */
    public Pais getEquipe1() {
        return equipe1;
    }

    /**
     * @param equipe1
     *            the equipe1 to set
     */
    public void setEquipe1(Pais equipe1) {
        this.equipe1 = equipe1;
    }

    /**
     * @return the equipe2
     */
    public Pais getEquipe2() {
        return equipe2;
    }

    /**
     * @param equipe2
     *            the equipe2 to set
     */
    public void setEquipe2(Pais equipe2) {
        this.equipe2 = equipe2;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Competicao)) {
            return false;
        }
        Competicao other = (Competicao) obj;
        if (id != other.id) {
            return false;
        }
        return true;
    }
}
