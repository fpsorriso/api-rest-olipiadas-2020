package br.com.cit.model;

import java.io.Serializable;

/**
 *Representa a etapa da competição
 */
public enum Etapa implements Serializable {
    ELIMINATORIAS("Eliminatórias", 1),
    OITAVAS("Oitavas de final", 2),
    QUARTAS("Quartas de final", 3),
    SEMI_FINAL("Semi-final",4),
    FINAL("Final", 5);
    
    private String descricao;
    private int id;
    
    /**
     * Cria um nova instancia da Etapa.
     * 
     * @param descricao
     * @param id
     */
    private Etapa(String descricao, int id) {
        this.descricao = descricao;
        this.id = id;
    }
    
    @Override
    public String toString() {
        return descricao;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }
 
    /**
     * Retorna a etapa de acordo com o ID.
     * 
     * @param id
     * @return a etapa de acordo com o ID.
     */
    public static Etapa valueOf(int id) {
        if (id > 0 && id < 6) {
            for (Etapa etapa : values()) {
                if (etapa.id == id) {
                    return etapa;
                }
            }
        }
        
        return null;
    }
}
