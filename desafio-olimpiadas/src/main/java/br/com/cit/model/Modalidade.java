package br.com.cit.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Representa a modalidade da competição.
 */
@Entity
public class Modalidade implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
    
    @Column(nullable=false, length=50)
    private String decricao;

    /**
     * Cria uma nova instancia da modalidade
     * 
     * @param id
     * @param decricao
     */
    public Modalidade(int id, String decricao) {
        this.id = id;
        this.decricao = decricao;
    }
    
    /**
     * Cria uma nova instancia da modalidade
     * 
     * @param decricao
     */
    public Modalidade(String decricao) {
        this(0, decricao);
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the decricao
     */
    public String getDecricao() {
        return decricao;
    }

    /**
     * @param decricao the decricao to set
     */
    public void setDecricao(String decricao) {
        this.decricao = decricao;
    }
}
