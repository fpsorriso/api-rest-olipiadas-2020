package br.com.cit.repository;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.data.annotation.Persistent;
import org.springframework.stereotype.Repository;

import br.com.cit.model.Local;

@Transactional
@Repository
public class LocalRepository implements IRepository<Local, Integer> {

	@Persistent
	private EntityManager entityManager;

	@Override
	public Local insere(Local entity) {
		if (entity != null) {
			entityManager.persist(entity);
		}

		return entity;
	}

	@Override
	public Local altera(Local entity) {
		if (entity != null) {
			entityManager.merge(entity);
			entityManager.flush();
		}

		return entity;
	}

	@Override
	public void remove(Integer pk) {
		if (pk != null) {
			entityManager.remove(entityManager.getReference(Local.class, pk));
		}

	}

	@Override
	public Local consulta(Integer pk) {
		if (pk != null) {
			return entityManager.find(Local.class, pk);
		}
		
		return null;
	}

	@Override
	public Collection<Local> consulta() {
		return entityManager.createQuery("FROM Local").getResultList();
	}

}
