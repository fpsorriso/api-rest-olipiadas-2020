package br.com.cit.repository;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.data.annotation.Persistent;
import org.springframework.stereotype.Repository;

import br.com.cit.model.Modalidade;

@Transactional
@Repository
public class ModalidadeRepository implements IRepository<Modalidade, Integer> {
	
	@Persistent
	private EntityManager entityManager;
	
	@Override
	public Modalidade insere(Modalidade entity) {
		if (entity != null) {
			entityManager.persist(entity);
		}
		
		return entity;
	}

	@Override
	public Modalidade altera(Modalidade entity) {
		if (entity != null) {
			entityManager.merge(entity);
			entityManager.flush();
		}
		
		return entity;
	}

	@Override
	public void remove(Integer pk) {
		if (pk != null) {			
			entityManager.remove(entityManager.getReference(Modalidade.class, pk));
		}		
	}

	@Override
	public Modalidade consulta(Integer pk) {
		if (pk != null) {
			return entityManager.find(Modalidade.class, pk);
		}
		
		return null;
	}

	@Override
	public Collection<Modalidade> consulta() {
		return entityManager.createQuery("FROM Modalidade").getResultList();
	}

}
