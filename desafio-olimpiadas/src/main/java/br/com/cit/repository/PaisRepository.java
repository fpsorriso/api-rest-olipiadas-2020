
package br.com.cit.repository;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.data.annotation.Persistent;
import org.springframework.stereotype.Repository;

import br.com.cit.model.Pais;

@Transactional
@Repository
public class PaisRepository implements IRepository<Pais, Integer> {

	@Persistent
	private EntityManager entityManager;
	
	@Override
	public Pais insere(Pais entity) {
		if (entity != null) {
			entityManager.persist(entity);
		}
		
		return entity;
	}

	@Override
	public Pais altera(Pais entity) {
		if (entity != null) {
			entityManager.merge(entity);
			entityManager.flush();
		}
		
		return entity;
	}

	@Override
	public void remove(Integer pk) {
		if (pk != null) {
			entityManager.remove(entityManager.getReference(Pais.class, pk));
		}
	}

	@Override
	public Pais consulta(Integer pk) {
		if (pk != null) {
			return entityManager.find(Pais.class, pk);
		}
		
		return null;
	}

	@Override
	public Collection<Pais> consulta() {
		return entityManager.createQuery("FROM Modalidade").getResultList();
	}

}
