package br.com.cit.repository;

import java.util.Collection;

/**
 * Inteface base para os metodos do repository.
 *
 * @param <T> Entity
 * @param <PK> identificado da Entity.
 */
public interface IRepository<T,PK> {
	/**
	 * Insere a entity na base de dados.
	 * 
	 * @param entity
	 * @return a entity.
	 */
    public T insere(T entity);
    
    /**
     * Altera a entity na base de dados.
     * 
     * @param entity
     * @return a entity
     */
    public T altera(T entity);
    
    /**
     * Remova a entity da base de dados.
     * 
     * @param pk
     */
    public void remove(PK pk);
    
    /**
     * Busca a entity pelo identificador.
     * 
     * @param pk
     * @return a entity.
     */
    public T consulta(PK pk);
    
    /**
     * Retorna a lista com todas as entity existente na base.
     * 
     * @return a lista com todas as entity existente na base.
     */
    public Collection<T> consulta();
}
