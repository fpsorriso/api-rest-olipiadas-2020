package br.com.cit.repository;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;

import javax.persistence.EntityManager;

import org.springframework.data.annotation.Persistent;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.cit.model.Competicao;
import br.com.cit.model.Local;
import br.com.cit.model.Modalidade;

@Transactional
@Repository
public class CompeticaoRespository implements IRepository<Competicao, Integer> {

	@Persistent
	private EntityManager entityManager;

	@Override
	public Competicao insere(Competicao entity) {

		if (entity != null) {
			entityManager.persist(entity);
		}

		return entity;
	}

	@Override
	public Competicao altera(Competicao entity) {
		if (entity != null) {
			entityManager.merge(entity);
			entityManager.flush();
		}

		return entity;
	}

	@Override
	public void remove(Integer pk) {
		if (pk != null) {
			entityManager.remove(pk);
		}
	}

	@Override
	public Competicao consulta(Integer pk) {
		if (pk != null) {
			return entityManager.find(Competicao.class, pk);
		}

		return null;
	}

	/**
	 * Consulta todas as competições podendo filtrar por modalidade.
	 * 
	 * @param modalidade
	 * @return todas as competições podendo filtrar por modalidade.
	 */
	public Collection<Competicao> consulta(Modalidade modalidade) {
		Collection<Competicao> result = new HashSet<>();

		String hql = new StringBuffer().append("  FROM Competicao as c").append(" WHERE ? is null OR c.modalidade = ?")
				.append(" ORDER BY c.dataInicio, c.dataFim").toString();

		result.addAll(entityManager.createQuery(hql).getResultList());

		return result;
	}

	@Override
	public Collection<Competicao> consulta() {
		return entityManager.createQuery("FROM Competicao").getResultList();
	}

	/**
	 * Verifica se existe uma competição da mesma modalidade no mesmo local e em perioado reentrante;
	 * 
	 * @param id
	 * @param modalidade
	 * @param local
	 * @param dataInicio
	 * @param dataFim
	 * @return true, se existe uma competição da mesma modalidade no mesmo local e em perioado reentrante;
	 */
	public boolean verificaCompeticaoDuplicata(int id, Modalidade modalidade, Local local, LocalDateTime dataInicio,
			LocalDateTime dataFim) {
		String hql = new StringBuffer().append("  FROM Competicao as c").append(" WHERE c.modalidade = :modalidade")
				.append("   AND c.local = :local").append("   AND c.id <> :id")
				.append("   AND ((c.dataInicio BETWEEN :dataInicio AND :dataFim)")
				.append("    OR  (c.dataFim BETWEEN :dataInicio AND :dataFim)")
				.append("    OR  (:dataInicio BETWEEN c.dataInicio AND c.dataFim)")
				.append("    OR  (:dataFim BETWEEN c.dataInicio AND c.dataFim))").toString();

		return !entityManager.createQuery(hql).setParameter("modalidade", modalidade).setParameter("local", local)
				.setParameter("id", id).setParameter("dataInicio", dataInicio).setParameter("dataFim", dataFim)
				.getResultList().isEmpty();
	}
}
